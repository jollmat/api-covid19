'use strict';
var mongoose = require('mongoose');

require('../models/ReportHistory');

var ReportHistory = mongoose.model('ReportHistory');

exports.getReports = function(req, res) {

    var covid19Api = require("covid19-api");
	
	covid19Api.getReports().then(reports => {
		console.log('');
		console.log('>>> Covid19Controller.getReports()', new Date());
		console.log('   -> Response sent on ' + new Date());
        res.setHeader('Content-Type', 'application/json');
        res.json(reports);
    } );
};

exports.getTravelHealthNotices = function(req, res) {

    var covid19Api = require("covid19-api");
	
	covid19Api.getTravelHealthNotices().then(travelHealthNotices => {
		console.log('');
		console.log('>>> Covid19Controller.getTravelHealthNotices()', new Date());
		console.log('   -> Response sent on ' + new Date());
        res.setHeader('Content-Type', 'application/json');
        res.json(travelHealthNotices);
    } );
};

exports.getFatalityRateByComorbidities = function(req, res) {

    var covid19Api = require("covid19-api");
	
	covid19Api.getFatalityRateByComorbidities().then(result => {
		console.log('');
		console.log('>>> Covid19Controller.getFatalityRateByComorbidities()', new Date());
		console.log('   -> Response sent on ' + new Date());
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    } );
};

exports.getFatalityRateBySex = function(req, res) {

    var covid19Api = require("covid19-api");
	
	covid19Api.getFatalityRateBySex().then(result => {
		console.log('');
		console.log('>>> Covid19Controller.getFatalityRateBySex()', new Date());
		console.log('   -> Response sent on ' + new Date());
		res.setHeader('Content-Type', 'application/json');
        res.json(result);
    } );
};

exports.getFatalityRateByAge = function(req, res) {

    var covid19Api = require("covid19-api");
	
	covid19Api.getFatalityRateByAge().then(result => {
		console.log('');
		console.log('>>> Covid19Controller.getFatalityRateByAge()', new Date());
		console.log('   -> Response sent on ' + new Date());
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    } );
};

exports.getReportHistories = function(req, res){
	ReportHistory.find(function(err, reports){
		if(err) res.send(500, err.message);
		
		console.log('');
		console.log('>>> Covid19Controller.getReportHistories()', new Date());
		console.log('   -> Response sent on ' + new Date());
		res.status(200).jsonp(reports);
	});
}

exports.addReportHistory = function(req, res){
	var reportHistoryInput = new ReportHistory({
        requestDate:    req.body.requestDate,
        report:     req.body.report
    });
    reportHistoryInput.save(function(err, history) {
		console.log('');
		console.log('>>> Covid19Controller.addReportHistory()', new Date());
		console.log('   -> Response sent on ' + new Date());
        if(err) return res.status(500).send( err.message);
		res.status(200).jsonp(history);
    });
}

exports.getReportsByCountries = function(req, res){
	var covid19Api = require("covid19-api");
	
	var countryName = req.params.name;
	
	try{
		covid19Api.getReportsByCountries(countryName).then(result => {
			console.log('');
			console.log('>>> Covid19Controller.getReportsByCountries()', new Date());
			console.log('   -> Response sent on ' + new Date());
		
			try{
				res.setHeader('Content-Type', 'application/json');
				res.json(result);
			} catch(err) {
				res.setHeader('Content-Type', 'application/json');
				res.json([]);
			}
		});
	} catch(err) {
		res.setHeader('Content-Type', 'application/json');
        res.json([]);
	}
	
}