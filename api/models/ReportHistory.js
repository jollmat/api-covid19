var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
	
var reportHistorySchema = new Schema({
  requestDate:    { type: String },
  report:     { type: String }
});
module.exports = mongoose.model('ReportHistory', reportHistorySchema);