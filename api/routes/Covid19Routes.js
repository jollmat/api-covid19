'use strict';

module.exports = function(app) {
	var covid19Controller = require('../controllers/Covid19Controller');
  
	app.route('/reports').get(covid19Controller.getReports);
	app.route('/travelHealthNotices').get(covid19Controller.getTravelHealthNotices);
	app.route('/fatalityRateByComorbidities').get(covid19Controller.getFatalityRateByComorbidities);
	app.route('/fatalityRateBySex').get(covid19Controller.getFatalityRateBySex);
	app.route('/fatalityRateByAge').get(covid19Controller.getFatalityRateByAge);
   
	app.route('/reports/history')
		.get(covid19Controller.getReportHistories)
		.post(covid19Controller.addReportHistory);
		
	app.route('/reports/history/save').post(covid19Controller.addReportHistory);
	
	app.route('/reports/country/:name').get(covid19Controller.getReportsByCountries);

};