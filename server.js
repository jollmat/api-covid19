var express = require('express'),
  app = express(),
  cors = require('cors'),
  appName = 'Covid19 API',
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser'),
  mongoose = require('mongoose'), 
  Schema = mongoose.Schema ;

app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(cors());

mongoose.connect('mongodb://localhost/reportHistory', function(err, res) {

	var routes = require('./api/routes/Covid19Routes'); //importing route
	routes(app); //register the route

	if(err) {
		console.log('ERROR: connecting to Database. ' + err);
    } else {
		console.log('');
		console.log('Connected to Database "reportHistory"!');

		// Start request and save reports process
		var covid19Api = require("covid19-api");
		
		let ReportHistoryModel = mongoose.model('ReportHistory');
		let reportHistoryInstance = new ReportHistoryModel();

		app.listen(port, function(){
			console.log('');
			console.log(appName + ' server started on: ' + port + ' with CORS enabled!');
		});

		covid19Api.getReports().then(reports => {
			reports = reports[0][0];
			reports.table = [];
			console.log('');
			console.log('Report obtained!', JSON.stringify(reports));

			reportHistoryInstance = new ReportHistoryModel();
			reportHistoryInstance.requestDate = new Date();
			reportHistoryInstance.report = JSON.stringify(reports);

			reportHistoryInstance.save(reports, function(err,res){
				console.log('');
				console.log('Report saved!', res);

				setInterval(() => {
					covid19Api.getReports().then(reports2 => {
						reports2 = reports2[0][0];
						reports2.table = [];
						console.log('');
						console.log('Report obtained!', JSON.stringify(reports2));

						reportHistoryInstance = new ReportHistoryModel();
						reportHistoryInstance.requestDate = new Date();
						reportHistoryInstance.report = JSON.stringify(reports2);
						
						reportHistoryInstance.save(reports2, function(err2, res2){
							console.log('');
							console.log('Report saved!', res2);
						});
					} );
				}, 60000 * 5); // 5 min
			});

		} );

	}
  
	
	
	app.get('/', function (req, res) {
	  res.status(200).json({
		  message: 'Welcome to the ' + appName
	  });
	});

	/*
	app.listen(port, function(){
		console.log('');
	  	console.log(appName + ' server started on: ' + port + ' with CORS enabled!');
	});
	*/
});

